<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResumesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resumes', function (Blueprint $table) {
            $table->increments('id');
            $table->text('covernotes')->nullable();
            $table->string('edulevel1')->nullable();
            $table->string('edulevel2')->nullable();
            $table->string('edulevel3')->nullable();
            $table->string('edulevel4')->nullable();
            $table->string('edutitle1')->nullable();
            $table->string('edutitle2')->nullable();
            $table->string('edutitle3')->nullable();
            $table->string('edutitle4')->nullable();
            $table->string('edufromyear1')->nullable();
            $table->string('edufromyear2')->nullable();
            $table->string('edufromyear3')->nullable();
            $table->string('edufromyear4')->nullable();
            $table->string('edutoyear1')->nullable();
            $table->string('edutoyear2')->nullable();
            $table->string('edutoyear3')->nullable();
            $table->string('edutoyear4')->nullable();
            $table->string('institution1')->nullable();
            $table->string('institution2')->nullable();
            $table->string('institution3')->nullable();
            $table->string('institution4')->nullable();
            $table->text('aboutedu1')->nullable();
            $table->text('aboutedu2')->nullable();
            $table->text('aboutedu3')->nullable();
            $table->text('aboutedu4')->nullable();
            $table->string('comp1')->nullable();
            $table->string('comp2')->nullable();
            $table->string('comp3')->nullable();
            $table->string('comp4')->nullable();
            $table->string('comp1title')->nullable();
            $table->string('comp2title')->nullable();
            $table->string('comp3title')->nullable();
            $table->string('comp4title')->nullable();
            $table->string('comp1cat')->nullable();
            $table->string('comp2cat')->nullable();
            $table->string('comp3cat')->nullable();
            $table->string('comp4cat')->nullable();
            $table->string('comp1type')->nullable();
            $table->string('comp2type')->nullable();
            $table->string('comp3type')->nullable();
            $table->string('comp4type')->nullable();
            $table->string('comp1loc')->nullable();
            $table->string('comp2loc')->nullable();
            $table->string('comp3loc')->nullable();
            $table->string('comp4loc')->nullable();
            $table->string('comp1salary')->nullable();
            $table->string('comp2salary')->nullable();
            $table->string('comp3salary')->nullable();
            $table->string('comp4salary')->nullable();
            $table->text('comp1duties')->nullable();
            $table->text('comp2duties')->nullable();
            $table->text('comp3duties')->nullable();
            $table->text('comp4duties')->nullable();
            $table->string('compfromyear1')->nullable();
            $table->string('compfromyear2')->nullable();
            $table->string('compfromyear3')->nullable();
            $table->string('compfromyear4')->nullable();
            $table->string('comptoyear1')->nullable();
            $table->string('comptoyear2')->nullable();
            $table->string('comptoyear3')->nullable();
            $table->string('comptoyear4')->nullable();
            $table->string('cert1')->nullable();
            $table->string('cert2')->nullable();
            $table->string('cert3')->nullable();
            $table->string('cert4')->nullable();
            $table->string('nxtjobtitle')->nullable();
            $table->string('nxtjobrelocate')->nullable();
            $table->string('nxtjobtype')->nullable();
            $table->string('nxtjobsalary')->nullable();
            $table->integer('user_id')->unsigned();
            $table->integer('area_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->boolean('live')->default(false);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resumes');
    }
}
