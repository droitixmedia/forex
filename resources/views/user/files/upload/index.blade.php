@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')




             <!-- breadcrumb begin -->
            <div class="breadcrumb-oitila db-breadcrumb">
                <div class="container">
                    <div class="row justify-content-lg-around">
                        <div class="col-xl-6 col-lg-7 col-md-5 col-sm-6 col-8">
                            <div class="part-txt">
                                <h1>Upload Proof of Payment</h1>
                                <ul>
                                    <li>Home</li>
                                    <li>Upload</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-5 col-md-7 col-sm-6 col-4 d-flex align-items-center">
                            <div class="db-user-profile">
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- breadcrumb end -->

            <!-- account begin -->
            <div class="user-dashboard">
                <div class="container">

                    

                    <div class="col-xl-7 col-lg-7 col-md-7">
                           
                            <div class="last-step">
                                <div>
                                <form action="/uploaddocs" method="post" enctype="multipart/form-data">
{{ csrf_field() }}

                                <div class="form-group">

                                            <input type="hidden" name="name" value="0" class="form-control">

                                        </div>


<br />
<input type="file" class="form-control" name="cvs[]" multiple />
<br />
<input type="submit" class="btn  btn-success" value="Upload Proof" />

</form>
                                </div>
                                <div class="calculation-content">
                                    <h4 class="title">Upload after you have paid</h4>
                                    <ul>
                                        
                                        
                                    </ul>
                                </div>
                            </div>
                           
                        </div>
                            
                        </div>
                    </div>


                   
                </div>
            </div>
            <!-- account end -->

@endsection
