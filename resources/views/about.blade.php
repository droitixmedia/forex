@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')




             <!-- breadcrumb begin -->
            <div class="breadcrumb-oitila db-breadcrumb">
                <div class="container">
                    <div class="row justify-content-lg-around">
                        <div class="col-xl-6 col-lg-7 col-md-5 col-sm-6 col-8">
                            <div class="part-txt">
                                <h1>Deposit</h1>
                                <ul>
                                    <li>Home</li>
                                    <li>Deposit</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-5 col-md-7 col-sm-6 col-4 d-flex align-items-center">
                            <div class="db-user-profile">
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- breadcrumb end -->

            <!-- account begin -->
            <div class="user-dashboard">
                <div class="container">

                    

                    <div class="col-xl-7 col-lg-7 col-md-7">
                           
                            <div class="last-step">
                                <div>
                                <label>Enter Your Amount</label>
                                    <form action="{{ route('listings.store', [$area]) }}" method="post">
                                    <span class="currency-symbol" id="basic-addon1">R</span>
                                    <input type="text" class="inputted-amount" name="amount" >
                                       
                                        
                               
                                <label>Select Your Desired % Return</label>
                                <div class="form-group">

                                        <select name="period" class="form-control">
                                       
                                     
                                        <option value="120% every 5 Days">120% every 5 Days</option>
                                        <option value="150% every 7 Days">150% every 7 Days</option>
                                       
                                       


                                        </select>
                                        </div>
                                   <input type="hidden" class="form-control" name="type" id="type" value="1">
                                           <input type="hidden" class="form-control" name="percent" id="percent" value="1.35">
                                         <input type="hidden" class="form-control" name="area_id" id="area" value="5">
                                         <input type="hidden" class="form-control" name="category_id" id="area" value="2">
                                 <div class="finish-button">
                                <button type="submit" class="btn-hyipox-2">Deposit</button>
                            </div>
                             {{ csrf_field() }}
                                </form>
                                </div>
                                <div class="calculation-content">
                                    <h4 class="title">Deposit Rules</h4>
                                    <ul>
                                        <li>
                                            <i class="far fa-check-circle"></i>
                                            <span class="list-title">
                                                Minimum
                                            </span>
                                            <span class="list-descr">P 1000</span>
                                        </li>
                                        <li>
                                            <i class="far fa-check-circle"></i>
                                            <span class="list-title">
                                                Maximum
                                            </span>
                                            <span class="list-descr">P 100,000</span>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                           
                        </div>
                            
                        </div>
                    </div>


                   
                </div>
            </div>
            <!-- account end -->

@endsection
