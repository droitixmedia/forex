              
@if(!$listing->matched())
             



 @elseif($listing->comments->count())                                   

  <div class="single-transaction" style="background-color: #087e8b;">
                                        <div class="flag">
                                            <img src="/uploads/avatars/{{ $listing->user->avatar }}" alt="">
                                        </div>
                                        <div class="user-info">
                                            <span class="name">{{$listing->user->name}} {{$listing->user->surname}}</span>
                                            <span class="tr-type">Trxn: Withdrawal</span>
                                            @foreach($listing->comments as $comment)
                                            <span class="tr-date">| {{$comment->created_at->diffForHumans()}}</span>
                                            @endforeach
                                            <span class="tr-amount">K{{$comment->split}}.00</span>
                                        </div>
                                        <div class="coin">
                                            <img src="assets/img/svg/money-bag.svg" alt="">
                                        </div>
                                    </div>

   @else

 <div class="single-transaction">
                                        <div class="flag">
                                          <img src="/uploads/avatars/{{ $listing->user->avatar }}" alt="">
                                        </div>
                                        <div class="user-info">
                                            <span class="name">{{$listing->user->name}} {{$listing->user->surname}}</span>
                                            <span class="tr-type">Trxn: Deposit</span>
                                            <span class="tr-date">| {{$listing->created_at->diffForHumans()}}</span>
                                            <span class="tr-amount">R{{$listing->amount}}.00</span>
                                        </div>
                                        <div class="coin">
                                            <img src="assets/img/svg/coin.svg" alt="">
                                        </div>
                                    </div>
   @endif