@extends('layouts.regapp')
@section('title', 'register')

@section('description')



@endsection
@section('content')
   

            <!-- breadcrumb begin -->
            <div class="breadcrumb-oitila">
                <div class="container">
                    <div class="row">
                        
                    </div>
                </div>
            </div>
            <!-- breadcrumb end -->

             <!-- login begin -->
            <div class="login">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-xl-8 col-lg-10 col-md-7 col-sm-9">
                            <div class="form-area">
                                <div class="row no-gutters">
                                    <div class="col-xl-6 col-lg-6">
                                        <div class="login-form">
                                           <form method="POST" class="form-horizontal" action="{{ route('login') }}">
                                          @csrf
                                                <div class="form-group">
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus >

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                    </div>
                                    <div class="form-group">
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>


                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                    </div>
                                                <div class="form-group form-check">
                                                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                    <label class="form-check-label" for="exampleCheck1">remember me</label>
                                                    <button class="btn-form">Submit</button>
                                                </div>
                                            </form>
                                            <div class="other-option">
                                                <a href="{{url('register')}}">register now</a>
                                                <a href="{{ route('password.request') }}">forgot password?</a>
                                            </div>
                                            <div class="login-by-social">
                                               
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 d-xl-block d-lg-block d-none">
                                        <div class="blank-space"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- login end -->

       
        
        

@endsection
