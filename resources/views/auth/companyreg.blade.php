@extends('layouts.regapp')
@section('title')
 Create Company Account  | Openjobs360
@endsection
@section('description')
Create Your Company Account and Start Posting Job Openings.
@endsection
@section('content')
    <!-- Inner Page Breadcrumb -->
    <section class="inner_page_breadcrumb bgc-f0 pt30 pb30" aria-label="breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="breadcrumb_title float-left">Create Company Account</h4>
                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Register</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <!-- Our LogIn Register -->
    <section class="our-log-reg bgc-fa">
        <div class="container">
            <div class="row">

                <div class="col-sm-12 col-lg-6">
                    <div class="sign_up_form">
                        <div class="heading">
                            <h3 class="text-center">Create Company Account</h3>
                            <p class="text-center">You have an account already? <a class="text-thm" href="{{ url('login') }}">Log in!</a></p>
                        </div>

                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                <form action="{{ route('register') }}" method="POST" autocomplete="off">
                                @csrf

                                    <div class="form-group">
                                      <input id="fullname" type="text" class="form-control{{ $errors->has('fullname') ? ' is-invalid' : '' }}" name="fullname" value="{{ old('fullname') }}" required autofocus placeholder="Company Name">

                                @if ($errors->has('fullname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('fullname') }}</strong>
                                    </span>
                                @endif
                                    </div>
                                     <div class="form-group">
                                       <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Email">

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                    </div>
                                    <input type="hidden"   name="truecompany" value="1">
                                    <div class="form-group">
                                        <label>Your Password (5 or more characters)</label>
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Choose a password">


                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                    </div>

                                    <div class="form-group">
                                        <label>Repeat password</label>
                     <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Type password again">

                                    </div>


                                    <button type="submit" class="btn btn-log btn-block btn-dark">Register</button>
                                    <hr>

                                </form>
                            </div>
                            <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                <form action="#">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="exampleInputName2" placeholder="User Name">
                                    </div>
                                     <div class="form-group">
                                        <input type="email" class="form-control" id="exampleInputEmail3" placeholder="Email">
                                    </div>
                                    <div class="form-group">
                                        <input type="number" class="form-control" id="exampleInputPhone2" placeholder="Phone Number">
                                    </div>
                                    <div class="form-group">
                                        <select id="inputState2" class="form-control">
                                            <option selected>Select Sector</option>
                                            <option>Web Developer</option>
                                            <option>Ui/Ux Designer</option>
                                            <option>Photoeditor</option>
                                            <option>Graphics Designer</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" id="exampleInputPassword3" placeholder="Password">
                                    </div>
                                    <div class="form-group form-check">
                                        <input type="checkbox" class="form-check-input" id="exampleCheck3">
                                        <label class="form-check-label" for="exampleCheck3">By Registering You Confirm That You Accept <a class="text-thm" href="page-terms-and-policies.html">Terms & Conditions</a> And <a class="text-thm" href="page-terms-and-policies.html">Privacy Policy</a></label>
                                    </div>
                                    <button type="submit" class="btn btn-log btn-block btn-dark">Register</button>
                                    <hr>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-6">
                    <div class="login_form">


                            <div class="heading">
                                <h3 class="text-center">Social Signup!</h3>
                                <p class="text-center">Do you have an account? <a class="text-thm" href="{{ url('login') }}">Login!</a></p>
                            </div>
                            <div class="row mt40">
                                        <div class="col-lg">
                                            <button type="submit" class="btn btn-block color-white bgc-fb"><i class="fa fa-facebook float-left mt5"></i> Facebook</button>
                                        </div>
                                        <div class="col-lg">
                                            <button type="submit" class="btn btn-block color-white bgc-gogle"><i class="fa fa-google float-left mt5"></i> Google</button>
                                        </div>
                                    </div>



                    </div>


                    </div>

                </div>
            </div>
        </div>
    </section>


@endsection
