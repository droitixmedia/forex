@extends('layouts.regapp')
@section('title', 'register')

@section('description')



@endsection
@section('content')
   

            <!-- breadcrumb begin -->
            <div class="breadcrumb-oitila">
                <div class="container">
                    <div class="row">
                        
                    </div>
                </div>
            </div>
            <!-- breadcrumb end -->

             <!-- login begin -->
            <div class="login">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-xl-8 col-lg-10 col-md-7 col-sm-9">
                            <div class="form-area">
                                  @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                                <div class="row no-gutters">
                                    <div class="col-xl-6 col-lg-6">
                                        <div class="login-form">
                                          <form method="POST" action="{{ route('password.email') }}">
                                @csrf
                                   <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Your email address" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                 
                                     <div class="form-group form-check">
                                                  
                                                    <button class="btn-form">Reset Password</button>
                                                </div>
                                    
                                </form>
                                <!-- /Form -->

                                            <div class="login-by-social">
                                               
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 d-xl-block d-lg-block d-none">
                                        <div class="blank-space"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- login end -->

       
        
        

@endsection
