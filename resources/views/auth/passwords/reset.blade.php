@extends('layouts.regapp')
@section('title', 'register')

@section('description')



@endsection
@section('content')
   

            <!-- breadcrumb begin -->
            <div class="breadcrumb-oitila">
                <div class="container">
                    <div class="row">
                        
                    </div>
                </div>
            </div>
            <!-- breadcrumb end -->

             <!-- login begin -->
            <div class="login">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-xl-8 col-lg-10 col-md-7 col-sm-9">
                            <div class="form-area">
                                  @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                                <div class="row no-gutters">
                                    <div class="col-xl-6 col-lg-6">
                                        <div class="login-form">
                       
                               <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group row">


                            <div class="col-lg-12">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus placeholder="Email">

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">


                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="New Password">

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">


                            <div class="col-md-12">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Confirm Password">
                            </div>
                        </div>

                            
                                <div class="form-group form-check">
                                                  
                                                    <button class="btn-form">Reset Password</button>
                                                </div>
                          
                       
                    </form>


                                            <div class="login-by-social">
                                               
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 d-xl-block d-lg-block d-none">
                                        <div class="blank-space"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- login end -->

       
        
        

@endsection
