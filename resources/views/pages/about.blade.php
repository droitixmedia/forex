@extends('layouts.userapp')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

            <!-- breadcrumb begin -->
            <div class="breadcrumb-oitila">
                <div class="container">
                    <div class="row justify-content-lg-around">
                        <div class="col-xl-9 col-lg-7 col-8">
                            <div class="part-txt">
                                <h1>About Techno Trading</h1>
                                <ul>
                                    <li>home</li>
                                    <li>about</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-4 d-flex align-items-center">
                            <div class="part-img">
                                <img src="assets/img/breadcrumb-img.png" alt="image">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- breadcrumb end -->

            <!-- about begin -->
            <div class="about about-inner">
                             <div class="container">
                    <div class="row justify-content-xl-between justify-content-lg-between justify-content-md-between justify-content-sm-center">
                        <div class="col-xl-6 col-lg-6 col-sm-10">
                            <div class="part-text">
                                <h2>Take <span class="special">advantage </span>of our years of forex trading experience.</h2>
                                <p>Once you create an investment order and its approved, your order will be added to its    <span class="spc">respective profit pool</span> and can be withdrawn instantly on maturity date.</p>
                                <ul>
                                    <li><i class="fas fa-check"></i> Withdrawals are everyday from 9am on your maturity date. </li>
                                    <li><i class="fas fa-check"></i> Our withdrawals are approved instantly.  </li>
                                    <li><i class="fas fa-check"></i> Your capital + Your profit is withrawable at once. </li>
                                    <li><i class="fas fa-check"></i> You can upgrade your shares to a higher plan anytime.</li>
                                </ul>
                                <p>We are taking advantage of high volume forex trading and operate 5 large accounts with 3 reputable forex brokers.</p>
                                <a href="{{ route('listings.create', [$area]) }}" class="btn-hyipox-2">Deposit Now</a>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-sm-10 col-md-12">
                            <div class="part-feature">
                                <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-sm-12 col-md-6">
                                        <div class="single-feature">
                                            <div class="feature-icon">
                                                <img src="assets/img/svg/solar-energy.svg" alt="">
                                            </div>
                                            <div class="feature-text">
                                                <h3>We Have an Elite Team</h3>
                                                <p>Our team of forex traders is well grounded and bags years of experience in the field </p>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-xl-12 col-lg-12 col-sm-12 col-md-6">
                                        <div class="single-feature">
                                            <div class="feature-icon">
                                                <img src="assets/img/svg/blockchain.svg" alt="">
                                            </div>
                                            <div class="feature-text">
                                                <h3>We Are Transparent</h3>
                                                <p>Every 5 Days we release our weekly perfomance statistics to our investors for any purpose they may deem usefull. </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-sm-12 col-md-6">
                                        <div class="single-feature">
                                            <div class="feature-icon">
                                                <img src="assets/img/svg/worldwide.svg" alt="">
                                            </div>
                                            <div class="feature-text">
                                                <h3>We're Global</h3>
                                                <p>Techno Trading is fast growing into a global forex profit sharing institution with operations in many countries</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- about end -->


           


@endsection
