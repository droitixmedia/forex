<div class="tradingview-widget-container">
  <div id="tradingview_4da96"></div>
  <div class="tradingview-widget-copyright"><a href="https://www.tradingview.com/symbols/NASDAQ-AAPL/" rel="noopener" target="_blank"><span class="blue-text">AAPL Chart</span></a> by TradingView</div>
  <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
  <script type="text/javascript">
  new TradingView.widget(
  {
  "autosize": true,
  "symbol": "NASDAQ:AAPL",
  "interval": "D",
  "timezone": "Etc/UTC",
  "theme": "dark",
  "style": "1",
  "locale": "en",
  "toolbar_bg": "#f1f3f6",
  "enable_publishing": false,
  "hide_side_toolbar": false,
  "allow_symbol_change": true,
  "watchlist": [
    "BINANCE:BTCUSDT",
    "OANDA:XAUUSD",
    "SP:SPX",
    "CURRENCYCOM:US30",
    "TVC:DXY",
    "OANDA:NAS100USD",
    "OANDA:SPX500USD",
    "CURRENCYCOM:US100",
    "FX:EURUSD",
    "OANDA:EURUSD",
    "OANDA:GBPUSD",
    "FX:GBPUSD",
    "NASDAQ:AAPL",
    "BITSTAMP:BTCUSD"
  ],
  "details": true,
  "hotlist": true,
  "calendar": true,
  "container_id": "tradingview_4da96"
}
  );
  </script>
</div>