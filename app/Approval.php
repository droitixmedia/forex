<?php

namespace openjobs;



use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Approval extends Model
{
      use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'body'];

    /**
     * The belongs to Relationship
     *
     * @var array
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function ownedByUser(User $user)
    {
        return $this->user->id === $user->id;
    }



       public function comment()
    {
        return $this->belongsTo(Comment::class);
    }


}
