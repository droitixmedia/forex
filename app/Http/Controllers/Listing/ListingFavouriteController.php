<?php

namespace openjobs\Http\Controllers\Listing;

use openjobs\{Area, Listing};
use Illuminate\Http\Request;
use openjobs\Http\Controllers\Controller;
use Auth;

class ListingFavouriteController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index(Request $request)
    {
        $user = Auth::user();

        $listings = $request->user()->favouriteListings()->with(['user', 'area'])->paginate(5);

        return view('user.listings.favourites.index', compact('listings','user'));
    }

    public function store(Request $request, Area $area, Listing $listing)
    {
        $request->user()->favouriteListings()->syncWithoutDetaching([$listing->id]);

       notify()->success('Job Ad added to favourites!');


        return back();
    }

    public function destroy(Request $request, Area $area, Listing $listing)
    {
        $request->user()->favouriteListings()->detach($listing);

        notify()->success('Job removed from favourites!');

        return back();
    }
}

