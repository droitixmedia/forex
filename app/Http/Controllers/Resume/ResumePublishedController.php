<?php

namespace openjobs\Http\Controllers\Resume;

use Illuminate\Http\Request;
use openjobs\Http\Controllers\Controller;
use Auth;

class ResumePublishedController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index(Request $request)
    {

        $user=Auth::user();
        $resumes = $request->user()->resumes()->with(['area'])->isLive()->latestFirst()->paginate(10);

        return view('user.resumes.published.index', compact('resumes','user'));
    }

    /**
     *
     *
     * @param area this is a side effect of the setup of the routes
     * @param category the category from which we want to extract listings
     * @return json response contain the listing in the given category
     * note that the response is json with the title an address of each listing always
     */
    public function resumesInCategory($area, $category)
    {

        $resumes = \openjobs\Resume::where('category_id', $category)
            ->isLive()
            ->limit(10)
            ->orderBy('updated_at', 'ASC')
            ->get(['title', 'address']);

        return response()->json(['success' => true, 'other_places' => $resumes]);
    }
}

