<?php

namespace openjobs\Http\Controllers;

use Auth;
use openjobs\Area;
use openjobs\Comment;
use openjobs\Listing;
use openjobs\Twilio\SendSmsNotification;
use openjobs\Category;
use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;
use openjobs\Approval;
use openjobs\Traits\Eloquent\{OrderableTrait, PivotOrderableTrait};


class HomeController extends Controller
{
     
    const INDEX_LIMIT = 10;
    public function index(Area $area, Category $category, Listing $listing, Comment $comment)
    {
        $listings = Listing::all()->where('live', true);

        $unpublishedcount = Auth::user()->listings()->where('live', false)->count();


    $referralink = 'https://technotrading.live/register/?ref=' . auth()->user()->id;



        return view('home', compact('listings', 'unpublishedcount', 'referralink'));
    }

     

   
    
    public function referral()
    {

    }

    public function referrer()
    {
        return auth()->user()->referrer;
    }

    public function referrals()
    {
        $referrals = auth()->user()->referrals()->paginate(1000);

        return view('referrals', compact('referrals'));
    }

    public function notifications()
    {
        return view('notifications');
    }

    public function markNotificationAsRead(DatabaseNotification $notification)
    {
        $notification->markAsRead();

        return response()->json([], 200);
    }
}
