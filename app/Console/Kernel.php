<?php

namespace openjobs\Console;

use openjobs\Listing;
use openjobs\Twilio\SendSmsNotification;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\Notification;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            Listing::whereNull('matured_at')->each(function ($listing) {
                $maturityDate = \Carbon\Carbon::parse($listing->updated_at->addHours($listing->days*24));

                if ($maturityDate->isPast()) {
                    $listing->matured_at = $maturityDate;
                    $listing->save();

                    Notification::send($listing->user, new InvestmentMaturedNotification($listing));

                    (new SendSmsNotification)->sendSms($listing->user->phone_number, 'Thank you. Your investment has matured. View it here: ' . route('listings.published.index'));
                }
            });
        })->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
