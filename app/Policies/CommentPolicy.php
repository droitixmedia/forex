<?php

namespace openjobs\Policies;

use openjobs\{User, Comment};
use Illuminate\Auth\Access\HandlesAuthorization;

class CommentPolicy
{
    use HandlesAuthorization;



    public function destroy(User $user, Comment $comment)
    {
        return $this->touch($user, $comment);
    }
     public function touch(User $user, Comment $comment)
    {
        return $comment->ownedByUser($user);
    }


}
