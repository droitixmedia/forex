# ndepapi

## About Transactions
## Set up .env config

```dotenv
MINIMUM_DEPOSIT_AMOUNT=1000
MAXIMUM_DEPOSIT_AMOUNT=100000
```
## About Mpesa Integration
## Set up .env config

```
SHORT_CODE=123456
LIPA_NA_MPESA_SHORT_CODE=654321
VALIDATION_KEY=some-secure-key
CONFIRMATION_KEY=some-secure-key
CONSUMER_SECRET=from-daraja
CONSUMER_KEY=from-daraja
LIPA_NA_MPESA_ONLINE_PASS_KEY=from-daraja
INITIATOR_NAME=from-daraja
SECURITY_CREDENTIAL=from-daraja


PROD_SHORT_CODE=123456
PROD_LIPA_NA_MPESA_SHORT_CODE=654321
PROD_VALIDATION_KEY=some-secure-key
PROD_CONFIRMATION_KEY=some-secure-key
PROD_CONSUMER_SECRET=from-daraja
PROD_CONSUMER_KEY=from-daraja
PROD_LIPA_NA_MPESA_ONLINE_PASS_KEY=from-daraja
PROD_INITIATOR_NAME=from-daraja
PROD_SECURITY_CREDENTIAL=from-daraja

```

##### Register C2B Endpoints
```
URI
/api/v1/c2b/register

Payload
--
```

##### Send dummy C2B transaction to app
```
URI
/api/v1/c2b/confirm/some_confirmation_key_as_in_dot_env_file

Payload
--
```

##### Send simulation request to Safaricom
```
URI
/api/v1/c2b/confirm/simulate

Payload

{
	"short_code":"short_code",
	"bill_ref_number":"some_account_number",
	"msisdn":"254_phone_number",
	"amount":"some_amount"
}
```
###STK Push
##### Send simulation request to Safaricom
```
URI
/api/v1/stk-push/simulate

Payload

{
    "sender_phone":"254_phone_number",
    "payer_phone":"254_phone_number",
    "amount":some_amount,
    "account_reference":"some_account_number"
}
```
##### Send dummy STK push transaction to app
```
URI
/api/v1/stk-push/confirm/some_confirmation_key_as_in_dot_env_file

Payload

	
{ "Body":
       { 
            "stkCallback":{ 
                "MerchantRequestID": "15578-16590447-1",
                "CheckoutRequestID": "ws_CO_010820202127420966",
                "ResultCode": "0",
                "ResultDesc": "0"
            } ,
           "CallbackMetadata":  {
            "Item": [
                    { "Name":"Amount", "Value":10 },
                    { "Name":"MpesaReceiptNumber",    "Value":"MRLSJHDH9" },
                    { "Name":"balance",    "Value":"1000" },
                    { "Name":"b2CUtilityAccountAvailableFunds",    "Value":"1000" },
                    { "Name":"transactionDate",    "Value":"vuala" },
                    { "Name":"phoneNumber",    "Value":"25472000000" }

            ]

         }
       }

} 
```
###B2C
##### Send B2C payment request to Safaricom
```
URI
/api/v1/b2c/pay

Payload

{
    "receiver_phone":"254_phone_number",
    "amount":some_amount,
    "account_reference":"some_account_number"
}
```
##### Send dummy B2C transaction to app
```
URI
/api/v1/b2c/confirm/some_confirmation_key_as_in_dot_env_file

Payload

	
{"Result": {
        "ResultType": 0,
        "ResultCode": 0,
        "ResultDesc": "The service request has been accepted successfully.",
        "OriginatorConversationID": "9076-23288485-1",
        "ConversationID": "AG_20210604_205013e5ba07e25c84a2",
        "TransactionID": "LGH3197RIB",
        "ResultParameters": {
            "ResultParameter": [
                {
                    "Key": "TransactionReceipt",
                    "Value": "LGH3197RIB"
                },
                {
                    "Key": "TransactionAmount",
                    "Value": 8000
                },
                {
                    "Key": "B2CWorkingAccountAvailableFunds",
                    "Value": 150000
                },
                {
                    "Key": "B2CUtilityAccountAvailableFunds",
                    "Value": 133568
                },
                {
                    "Key": "TransactionCompletedDateTime",
                    "Value": "17.07.2017 10:54:57"
                },
                {
                    "Key": "ReceiverPartyPublicName",
                    "Value": "254708374149 - John Doe"
                },
                {
                    "Key": "B2CChargesPaidAccountAvailableFunds",
                    "Value": 0
                },
                {
                    "Key": "B2CRecipientIsRegisteredCustomer",
                    "Value": "Y"
                }
            ]
        },
        "ReferenceData": {
            "ReferenceItem": {
                "Key": "QueueTimeoutURL",
                "Value": "https://internalsandbox.safaricom.co.ke/mpesa/b2cresults/v1/submit"
            }
        }
    }
}

```


